# DM Group common utilities and functions

## Deployer common tasks 

adds tasks to:

- configure nginx and apache

naming conventions: `repo-name`_`stage`.conf

- reload php fpm (7.2 or 7.3)