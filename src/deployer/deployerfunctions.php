<?php
namespace Deployer;

desc('Adds Nginx vhost and restarts server');
task('server:nginx-config-vhost', function () {
    if (input()->hasArgument('stage')) {
        $stage = input()->getArgument('stage');
    } else {
        write('Cant continue, undefined stage');
    }

    if (!checkAndCreateConf(get('application'),"nginx")) {
        writeln("Can't continue, config files missing. Scaffolding created.");
    } else {
        writeln("Removing previous configuration file if present...");
        run("sudo rm -f /etc/nginx/sites-enabled/{{application}}_$stage.conf");
        run("sudo rm -f /etc/nginx/sites-available/{{application}}_$stage.conf");

        writeln("Copying configuration file...");
        run("sudo cp {{release_path}}/config/webserver/nginx/{{application}}_$stage.conf /etc/nginx/sites-available/");

        writeln("Linking configuration file...");
        run("sudo ln -s /etc/nginx/sites-available/{{application}}_$stage.conf /etc/nginx/sites-enabled/");

        writeln("Restarting webserver...");
        run('sudo systemctl restart nginx.service');
    }
});

desc('Adds Apache2 vhost and restarts server');
task('server:apache-config-vhost', function () {
    if (input()->hasArgument('stage')) {
        $stage = input()->getArgument('stage');
    } else {
        write('Cant continue, undefined stage');
    }

    if (!checkAndCreateConf(get('application'),"apache")) {
        write('Cant continue, config files missing. Scaffolding created.');
    } else {
        write("Removing previous configuration file if present...");
        run("sudo rm -f /etc/apache2/sites-enabled/{{application}}_$stage.conf");
        run("sudo rm -f /etc/apache2/sites-available/{{application}}_$stage.conf");

        writeln("Copying configuration file...");
        run("sudo cp {{release_path}}/config/webserver/apache/{{application}}_$stage.conf /etc/apache2/sites-available/");

        write("Linking configuration file...");
        run("sudo ln -s /etc/apache2/sites-available/{{application}}_$stage.conf /etc/apache2/sites-enabled/");

        writeln("Restarting webserver...");
        run('sudo systemctl restart apache2.service');
    }
});

desc('restarts php fpm module (7.2 to 7.4)');
task('server:restart-php-fpm', function () {
    run("#!/bin/sh \n if [ -f /etc/init.d/php7.4-fpm ]; then sudo /etc/init.d/php7.4-fpm reload; fi");
    run("#!/bin/sh \n if [ -f /etc/init.d/php7.3-fpm ]; then sudo /etc/init.d/php7.3-fpm reload; fi");
    run("#!/bin/sh \n if [ -f /etc/init.d/php7.2-fpm ]; then sudo /etc/init.d/php7.2-fpm reload; fi");
});

desc('Restart supervisor');
task('server:restart-supervisor', function () {
    run('sudo /etc/init.d/supervisor restart');
});

/**
 * @param $application
 * @param $server
 * @return true | false
 */
function checkAndCreateConf($application, $server)
{
    $result = true;
    $config_staging     = "config/webserver/$server/{$application}_staging.conf";
    $config_production  = "config/webserver/$server/{$application}_production.conf";
    $placeholder       = <<<EOT
    server {
        server_name {{domain}};
        listen 80;

        access_log /mnt/hosting/log/{{domain}}-access.log;
        error_log /mnt/hosting/log/{{domain}}-error.log error;

        root /mnt/hosting/sites/{{release}}_{{stage}}/current/public;

        add_header X-Frame-Options "SAMEORIGIN";
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Content-Type-Options "nosniff";

        index index.html index.htm index.php;

        charset utf-8;

        location / {
            auth_basic           “Administrator’s Area”;
            auth_basic_user_file /mnt/hosting/passwd/.htpasswd; 
            try_files \$uri \$uri/ /index.php?\$query_string;
        }

        location = /favicon.ico { access_log off; log_not_found off; }
        location = /robots.txt  { access_log off; log_not_found off; }

        error_page 404 /index.php;

        location ~ \.php$ {
              fastcgi_pass unix:/run/php/php7.3-fpm.sock;
              include fastcgi_params;
              fastcgi_param SCRIPT_FILENAME \$document_root/index.php;
              fastcgi_param HOST \$host;
              fastcgi_param HTTP_X_PROTOCOL https;

        }

        location ~ /\.(?!well-known).* {
            deny all;
        }
    }
EOT;

    if (!file_exists("config")) mkdir("config");
    if (!file_exists("config/webserver")) mkdir("config/webserver");
    if (!file_exists("config/webserver/$server")) mkdir("config/webserver/$server");

    if (!is_file($config_staging)) {
        file_put_contents($config_staging, $placeholder);
        $result = false;
    }
    if (!is_file($config_production)) {
        file_put_contents($config_production, $placeholder);
        $result = false;
    }
    return $result;
}